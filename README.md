#simpleness-ioc
spring 越来越重，在一些简单的项目中只想使用 spring 的ioc 功能还得导入很多其他的不必要的jar很是麻烦，
所以就重复造了一个轮子，简单的ioc注入功能。

##目录结构
    1.simpleness-ioc  项目主目录
    2.demo            测试项目目录
##用法
    BeanUtils.beanScan();   开启类扫码，默认扫码项目根目录下的所有类

    BeanUtils.beanScan("com.cyy"); 开启类扫描，扫描指定的包以及以下的包

    类上增加 @Component 注解，标识该类会注册成一个主键，在需要的地方增加

    使用的时候在属性上增加 @Resource (或者 @Autowired) 标识需要注入

##例如
```java
    @Component   //  UserBizImpl 注册成一个组件
    public class UserBizImpl implements UserBiz{

        @Resource  // 这里使用  @Resource 标识 标识该属性需要注入
	    private UserDao userDao;

    	@Override
    	public String findById(Long id) {
    		return userDao.findById(id);
    	}
    }
```
