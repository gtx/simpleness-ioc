package com.cyy.demo.biz.impl;

import javax.annotation.Resource;

import com.cyy.demo.biz.UserBiz;
import com.cyy.demo.dao.UserDao;
import com.cyy.ioc.annotation.Component;

@Component
public class UserBizImpl implements UserBiz{

	@Resource
	private UserDao userDao;

	@Override
	public String findById(Long id) {
		return userDao.findById(id);
	}

	
}
