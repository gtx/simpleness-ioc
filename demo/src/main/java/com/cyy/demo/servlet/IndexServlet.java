package com.cyy.demo.servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cyy.demo.biz.UserBiz;
import com.cyy.demo.biz.impl.UserBizImpl;
import com.cyy.ioc.tools.BeanUtils;

public class IndexServlet extends HttpServlet{

	private static final long serialVersionUID = -6894053500367908679L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		UserBiz biz = BeanUtils.getBean(UserBiz.class);
		UserBiz biz2 = BeanUtils.getBean(UserBizImpl.class);
		
		String user1 = biz.findById(1l);
		String user2 = biz2.findById(2l);
		
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:sss"); 
		StringBuilder str = new StringBuilder("request is ok\r\n");
		str.append("date time :").append(format.format(new Date()))
		.append("\r\n")
		.append("user1:").append(user1)
		.append("\r\n")
		.append("user2:").append(user2);
		
		resp.getWriter().println(str);
		resp.getWriter().close();
	}
	

}
