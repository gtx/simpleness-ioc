package com.cyy.ioc.tools;

import com.cyy.ioc.exceptions.BeanInitException;

/**
 * 规定了bean 的一些信息
 * @author Administrator
 */
public class BeanInfo {

	public BeanInfo(String cls,boolean isSingle,Object data){
		this.cls = cls;
		this.isSingle = isSingle;
		this.data = data;
	}
	
	private String cls;
	
	private boolean isSingle;
	
	private Object data;
	
	public Object getData(){
		if(StringUtils.isEmpty(this.cls)){
			throw new BeanInitException("bean class is empty.");
		}
		if(data == null || isSingle == false){
			try {
				data = Class.forName(this.cls).newInstance();
			} catch (Exception e) {
				e.printStackTrace();
				throw new BeanInitException("bean ["+this.cls+"] init is error.",e);
			}
			BeanUtils.injectBean(this.data);
			BeanUtils.putBean(this.data,this.isSingle);
		}
		return this.data;
	}
	
	public Object getOriginalData(){
		return this.data;
	}

	public String getCls() {
		return cls;
	}

	public void setCls(String cls) {
		this.cls = cls;
	}

	public boolean isSingle() {
		return isSingle;
	}

	public void setSingle(boolean isSingle) {
		this.isSingle = isSingle;
	}
	
}
