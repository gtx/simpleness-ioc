package com.cyy.ioc.tools;

import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.management.RuntimeErrorException;

public final class StringUtils {

	public static final boolean isEmpty(String str){
		return str == null || str.trim().length() == 0;
	}
	public static final boolean isNotEmpty(String str){
		return !isEmpty(str);
	}
	
	public static final String encode(String str){
		if(isEmpty(str)){return "";}
		try{
			return URLEncoder.encode(str,Constant.CHARSET);
		}catch (Exception e){
			throw new RuntimeException(e);
		}
	}
	public static final String decoder(String str){
		if(isEmpty(str)){return "";}
		try{
			return URLDecoder.decode(str,Constant.CHARSET);
		}catch(Exception e){
			throw new RuntimeException("url 解码错误",e);
		}
	}
	
	public static final void printUrl(URL url){
		System.out.println("toString:"+url.toString());
		System.out.println("protocol:"+url.getProtocol());
		System.out.println("host:"+url.getHost());
		System.out.println("file:"+url.getFile());
		System.out.println("authority:"+url.getAuthority());
		System.out.println("query:"+url.getQuery());
		System.out.println("ref:"+url.getRef());
		System.out.println("userinfo:"+url.getUserInfo());
	}
}
