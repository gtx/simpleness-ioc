package com.cyy.ioc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)  // 用于描述类、接口、enum
@Retention(RetentionPolicy.RUNTIME) // 运行时有效
@Inherited
public @interface Component {
	
	/**
	 * 是否单例
	 */
	boolean single = true;	
}
