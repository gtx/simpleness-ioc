package com.cyy.ioc.exceptions;

public class BeanInitException extends RuntimeException{

	/**
	 */
	private static final long serialVersionUID = -516879383046315865L;

	public BeanInitException(String msg,Throwable e){
		super(msg,e);
	}
	
	public BeanInitException(String msg){
		super(msg);
	}
}
