package com.cyy.ioc.exceptions;

public class InjectBeanException extends RuntimeException{

	private static final long serialVersionUID = 8734776966085923146L;
	
	public InjectBeanException(Throwable e){
		super(e);
	}
	public InjectBeanException(String msg,Throwable e){
		super(msg,e);
	}
	public InjectBeanException(String msg){
		super(msg);
	}
}
